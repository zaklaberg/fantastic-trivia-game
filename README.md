# fantastic-trivia-game

A trivia game using the Vue framework.
The player can adjust difficulty, category and number of questions.
The game uses the https://opentdb.com/api_config.php API for trivia questions.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
