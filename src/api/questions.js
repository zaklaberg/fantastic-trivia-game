
let cachedQuestions = null
let CATEGORY_ANY_ID = -1
let settings = {
    amount: 10,
    category: CATEGORY_ANY_ID,
    difficulty: 'any',
    type: 'any'
}

function getAvailableDifficulties() {
    return ['any', 'easy', 'medium', 'hard']
}

function setSettings(newSettings) {
    settings = { ...settings, ...newSettings }

    // Error checking
    const validSettingKeys = ['amount', 'category', 'difficulty', 'type']
    Object.keys(settings).forEach(setting => {
        if (!validSettingKeys.includes(setting)) {
            throw new Error(`The supplied setting: ${setting} is not valid`)
        }
    })
}

function getSettings() {
    return {...settings}
}

async function fetchQuestions() {
    const TRIVIA_API_URL = 'https://opentdb.com/api.php'
    
    // Build query string
    let queryString = []
    Object.keys(settings).forEach(setting => {
        // To get all alternatives for a setting, omit from query string
        if (settings[setting] === 'any' || settings[setting] === -1) return
        queryString.push(`${setting}=${settings[setting]}`)
    })
    queryString = queryString.join('&')

    // Contact server
    try {
        const resp = await fetch(`${TRIVIA_API_URL}?${queryString}`)
        const questions = await (await resp.json()).results
 
        questions.forEach(q => {
            q.all_answers = [...q.incorrect_answers, q.correct_answer]
                                .map((e,i) => ({answer: e, id: i}))
            q.correct_answer_id = q.all_answers.length - 1
        })
        
        cachedQuestions = questions
        return questions
    }
    catch(e) {
        alert('Unable to get questions from server')
        return []
    }
}

// If null, maybe fetch? Or leave to user.
function fetchLastQuestions() {
    return cachedQuestions;
}

async function fetchCategories() {
    const TRIVIA_CATEG_URL = 'https://opentdb.com/api_category.php'

    try {
        const resp = await fetch(TRIVIA_CATEG_URL)
        return [{id: CATEGORY_ANY_ID, name: 'Any'}, ...(await resp.json()).trivia_categories]
    }
    catch(e) {
        alert('Unable to get categories from server.')
        return []
    }
}

export default {
    fetchQuestions,
    fetchCategories,
    fetchLastQuestions,
    setSettings,
    getSettings,
    getAvailableDifficulties
}