import VueRouter from 'vue-router'
import Start from './components/Start.vue'
import Game from './components/Game.vue'
import Settings from './components/Settings.vue'
import Results from './components/Results.vue'
import NotFound from './components/NotFound.vue'

const routes = [{
    path: '/',
    name: 'Start',
    component: Start
},
{
    path: '/Start',
    redirect: '/'
},
{
    path: '/Game',
    name: 'Game',
    component: Game
},
{
    path: '/Settings',
    name: 'Settings',
    component: Settings
},{
    path: '/Results',
    name: 'ResultsEmpty',
    component: NotFound
},
{
    path: '/Results/:answers',
    name: 'Results',
    component: Results
},
{
    path: '*',
    name: 'NotFound', 
    component: NotFound
}
]

const router = new VueRouter({ routes })

export default router;

